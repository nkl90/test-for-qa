<?php

namespace App\DataFixtures;

use App\Entity\LearningProgram;
use App\Entity\Lesson;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;

class LearningProgramFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * @throws Exception
     */
    public function load(ObjectManager $manager): void
    {
        $userRepo = $manager->getRepository(User::class);
        $userList = $userRepo->findAll();

        $lessonRepo = $manager->getRepository(Lesson::class);
        $lessonList = $lessonRepo->findAll();

        /** @var User $user */
        foreach ($userList as $user) {
            for ($i = 1; $i < random_int(2, 5); $i++) {
                $lessonsCount = random_int(3, 10);
                $learningProgram = new LearningProgram();
                $learningProgram->setName("Учебная программа #$i");
                $learningProgram->setStudent($user);
                $lessonKeys = [];
                while ($lessonsCount !== 0) {
                    $lessonKey = random_int(0, count($lessonList)-1);
                    if (!in_array($lessonKey, $lessonKeys, true)) {
                        /** @var Lesson $lesson */
                        $lesson = $lessonList[$lessonKey];
                        $learningProgram->addLesson($lesson);
                        $lessonKeys[] = $lessonKey;
                        $lessonsCount--;
                    }
                }
                $manager->persist($learningProgram);
            }
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            LessonFixtures::class,
        ];
    }
}
