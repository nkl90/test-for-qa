<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordEncoder;

    public function __construct(UserPasswordHasherInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager): void
    {

        $user = new User();

        $user->setEmail('admin@test.test');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword($this->passwordEncoder->hashPassword($user, 'test_password'));

        $manager->persist($user);

        $user2 = new User();

        $user2->setEmail('user@test.test');
        $user2->setRoles([]);
        $user2->setPassword($this->passwordEncoder->hashPassword($user2, 'test_password'));

        $manager->persist($user2);

        $manager->flush();
    }
}
