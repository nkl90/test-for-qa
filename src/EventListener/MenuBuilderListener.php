<?php

namespace App\EventListener;

use Sonata\AdminBundle\Event\ConfigureMenuEvent;

class MenuBuilderListener
{
    public function addMenuItems(ConfigureMenuEvent $event): void
    {
        $menu = $event->getMenu();
        $menu->addChild('exit', [
            'label' => 'Вернуться на платформу',
            'route' => 'app_index',
        ]);
    }
}