<?php

namespace App\Controller;

use App\Entity\Lesson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class LessonController extends AbstractController
{
    /**
     * @Route("/lesson", name="app_lesson_list")
     */
    public function list(): Response
    {
        $lessonRepo = $this->getDoctrine()->getRepository(Lesson::class);

        $lessonList = $lessonRepo->findAll();

        $lessons = [];

        /** @var Lesson $lesson */
        foreach ($lessonList as $lesson) {
            $lessons[$lesson->getType()][] = $lesson;
        }

        return $this->render('lesson/list.html.twig', [
            'lessonTypes' => Lesson::LESSON_TYPES,
            'lessons' => $lessons,
            'title' => 'База занятий',
            'controller_name' => 'LessonController',
        ]);
    }
    /**
     * @Route("/lesson/{id<\d+>}", name="app_lesson")
     */
    public function index(int $id): Response
    {
        $lessonRepo = $this->getDoctrine()->getRepository(Lesson::class);

        /** @var Lesson $lesson */
        $lesson = $lessonRepo->find($id);

        if (!$lesson) {
            throw new NotFoundHttpException('Lesson not found');
        }

        return $this->render('lesson/index.html.twig', [
            'title' => $lesson->getName(),
            'lesson' => $lesson,
            'controller_name' => 'LessonController',
        ]);
    }
}
