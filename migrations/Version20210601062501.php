<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210601062501 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE learning_program_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE learning_program (id INT NOT NULL, student_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_564ACF77CB944F1A ON learning_program (student_id)');
        $this->addSql('CREATE TABLE learning_program_lesson (learning_program_id INT NOT NULL, lesson_id INT NOT NULL, PRIMARY KEY(learning_program_id, lesson_id))');
        $this->addSql('CREATE INDEX IDX_3413C122ED94D8BC ON learning_program_lesson (learning_program_id)');
        $this->addSql('CREATE INDEX IDX_3413C122CDF80196 ON learning_program_lesson (lesson_id)');
        $this->addSql('ALTER TABLE learning_program ADD CONSTRAINT FK_564ACF77CB944F1A FOREIGN KEY (student_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE learning_program_lesson ADD CONSTRAINT FK_3413C122ED94D8BC FOREIGN KEY (learning_program_id) REFERENCES learning_program (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE learning_program_lesson ADD CONSTRAINT FK_3413C122CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE learning_program_lesson DROP CONSTRAINT FK_3413C122ED94D8BC');
        $this->addSql('DROP SEQUENCE learning_program_id_seq CASCADE');
        $this->addSql('DROP TABLE learning_program');
        $this->addSql('DROP TABLE learning_program_lesson');
    }
}
